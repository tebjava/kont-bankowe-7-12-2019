package glowna;

public class Main {
	public static void main(String... args) {
		//poniższa linia kodu tworzy obiekt z klasy KontoBankowe
		//tworzenie w pamięci obiektu jest wykonywane poprzez operator new 
		KontoBankowe kb = new KontoBankowe();
		
		kb.setPosiadacz("Janusz", "Tokarski"); //dodanie imienia i nazwiska danej osoby
		System.out.println(kb.getPosiadacz()); //sposób na wyświetlenie danych
		kb.dodajSaldo(2500); //dodanie do konta 2000 zł
		System.out.println(kb.pokazSaldo());
		kb.dodajSaldo(234.67); //kolejne doładowanie
		System.out.println(kb.pokazSaldo());
		kb.odejmijSaldo(4000); //proba odjęcia salda - wywoła błąd -> false
		System.out.println(kb.pokazSaldo());
		kb.odejmijSaldo(2000); 
		System.out.println(kb.pokazSaldo());
		kb.odejmijSaldo(500); //proba odjęcia salda - wywoła błąd -> false
		System.out.println(kb.pokazSaldo());
		kb.odejmijSaldoDebet(500); //to też nie zadziała -> maskDebet wynosi 0 zł (nie było wywołania odpowiedniej metody w Kontobankowe)
		System.out.println(kb.pokazSaldo());
		kb.setPosiadacz("Nowa nazwa"); //została wywołana metoda, która ustawia maksymalny debet na koncie w wysokości -2000 zł!
		kb.dodajSaldo(2500); //dodanie do konta 2000 zł
		System.out.println(kb.pokazSaldo());
		kb.dodajSaldo(234.67); //kolejne doładowanie
		System.out.println(kb.pokazSaldo());
		kb.odejmijSaldo(4000); //proba odjęcia salda - wywoła błąd -> false
		System.out.println(kb.pokazSaldo());
		kb.odejmijSaldo(2000); 
		System.out.println(kb.pokazSaldo());
		kb.odejmijSaldo(500); //proba odjęcia salda - wywoła błąd -> false
		System.out.println(kb.pokazSaldo());
		kb.odejmijSaldoDebet(500); //teraz ta metoda zadziała - będziemy mieć ujemną wartość salda!
		System.out.println(kb.pokazSaldo());
	}
}
