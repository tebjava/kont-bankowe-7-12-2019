//w programach Javy w dobrym tonie jest utrzymywanie kodu w paczkach
//(lepsze zarządzanie kodem)
package glowna;

/*
 * Klasa przechowująca informacje na temat konta bankowego. Klasy to nic innego jak 
 * próba odwzorowania naszej rzeczywistości w programie komputerowym. Klasy posiadają 
 * najczęściej szczegółowe, jak na nasze potrzeby, właściwości (zmienne -> pola) 
 * opisujące dany odwzorowywany fragment naszej rzeczywistości (np. klasa osoba posiadac będzie
 * np. imie, nazwisko, kolor skóry, kolor włosów, kolor oczów, wzrost, wagę, płeć itp.), które są 
 * istotne dla pisanego programu (przykładowo dla dietetyka). Niekiedy mogą posiawić się
 * dodatkowe właściwości, jak ulubiony kolor czy też wyznanie lecz będzie to sprawa drugorzędna, z punktu
 * widzenia programu dla dietetyki (lecz może być potrzebne dla jakiejś organizacacji, do której
 * dana osoba chce przystąpić). 
 * 
 * Jak więc można wywnioskować, klasa jest tak naprawdę ZMIENNĄ ZŁOŻONĄ, posiadającą wiele zmiennych
 * zarówno prostych (np. int,float,double,boolean) jak i złożonych (np. String, nasze własne klasy).
 */
public class KontoBankowe {
	/*
	 * Każda składowa (składowe - ogólne określenie zmiennych i funkcji w klasie) może posiadać 
	 * różne zakresy widoczności w kodzie. Dzięki widoczności programista może ukrywać (bądz pokazywać)
	 * określone składowe w kodzie aplikacji; to z kolei wpływa na to, czy w innych fragmentach kodu
	 * (w których tworzone będą obiekty na podstawie klasy) będzie możliwość pracy na określonych 
	 * składowych, czy też taką możliwość będą miały jedynie pozostałe składowe należące do tej klasy
	 * (zostanie to wyjaśnione pózniej, na przykładzie)
	 * 
	 * W przypadku Javy mamy następujące zakresy widoczności:
	 * - private - każda składowa tego typu może być odczytywana i ustawiane jedynie przez
	 * kod w samej klasie (np. w którejkolwiek metodzie), nie może zaś być wywoływana i/lub zmieniana
	 * przez programistę np. w funkcji main (w której utwozony zostanie obiekt z naszej klasy).
	 * 
	 * - public - wskazana składowa jest widoczna wszędzie tak samo; każdy fragment kodu może ją odczytywać
	 * i modyfikować
	 * 
	 * - protected - składowa traktowana jest jak prywatna jednak w przypadku, gdy dana klasa stanowi
	 * podstawę nowej klasy (tzw. dziedziczenie - omówione pózniej) to składowa tego typu dla klasy
	 * dziedziczącej będzie widziana jako public
	 * 
	 * Domyślnie Java tworzy składowe jako tzw. internal; w danym projekcie są one widziane jako public,
	 * poza nim jako private (w dokumentacji jest to nazwana jako package-private). 
	 */
	private String nazwaSkrocona,
			imie,
			nazwisko,
			nazwa,
			miejscowosc,
			ulica,
			kodPocztowy,
			numerPosesja,
			numerLokal,
			dowodTozsamosci,
			telefon,
			nrKonta;
	private double saldo, maksDebet;
	
	/*
	 * Przykład metody (tak nazywane są w klasach funkcje), która wyświetla nam nie pojedyncze dane posiadacza
	 * rachunku a całe dane (imię, nazwisko, adres zamieszkania)
	 * Metoda ta ma dostęp do pól prywatnych (ponieważ jej kod zawiera się w klasie)
	 */
	public String getPosiadacz() {
		String zw = imie + " " + nazwisko + ", zamieszkały w " +
	kodPocztowy + " " + miejscowosc + ", ul. " + ulica +
	" " + numerPosesja;
		try {
		if (!numerLokal.isEmpty())
			//zw = zw + " lok. " + numer_lokal;
			zw += " lok. " + numerLokal;
		}
		catch (Exception e) {
			
		}
		return zw;
	}
	/*
	 * Poniższa metoda pozwala na wprowadzenie do obiektu (zaalokowana klasa w przestrzeni pamięci operacyjnej - RAM)
	 * imienia oraz nazwiska posiadacza racunku. Należy tutaj zwrócić uwage na tzw. pseudozmienną this. this nazywane jest
	 * pseudozmienną ponieważ nie jest ona przez nikogo deklarowana; nie jest też przez nikogo definiowana. Powstaje ona
	 * w chwili, gdy w programie Java deklarowany jest obiekt wskazanej klasy. this pozwala na odwołanie się obiektowi
	 * do własnych składowych, do których normalnie nie mógłby się odwołać/miałby problem by odwołać się do nich np.
	 * przez nałożenie się nazw zmiennych i pól. Samo this przechowuje numer komórki pamięci, od której obiekt jest
	 * zaalokowany (usytuowany). 
	 * 
	 * W poniższym przykładzie składowe z this otrzymują przypisanie wartości, jakie zostaną wprowadzone przez
	 * parametry imie i nazwisko
	 */
	public boolean setPosiadacz(String imie, String nazwisko) {
		this.imie = imie;
		this.nazwisko = nazwisko;
		return true;
	}
	
	/*
	 * Metoda dodająca nazwę skróconą dla posiadacza rachunku; W języku Java bardzo powszene jest
	 * nadpisywanie funkcji (tzw. przeciążenie). Dzięki przeciążeniom mamy możliwość tworzenia funkcji/metod
	 * o takich samych nazwach, które wykonują różne czynności (w zależności od padanych przez nas parametrów)
	 * Niekiedy tzw. przeciążone metody mogą nawet zwracać inne typy danych. 
	 * 
	 * W przeciwieństwie do poprzedniej metody, poniżej zadeklarowana nie uzupełnia bezpośrednio określonego
	 * pola o podaną wartość; odwołuje się ona natomiast do tak samo nazwanej innej metody, która posiada znacznie
	 * więcej parametrów wejściowych. Metoda ustawia nazwę skróconą, pozostałe zaś zamienia na null
	 */
	public boolean setPosiadacz(String nazwaSkrocona) {
		
		setPosiadacz(nazwaSkrocona,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null);
		return true;
	}
	
	/*
	 * Kolejna przeciążona metoda; jej zadaniem jest wstawienie nazwy skrócone, imienia oraz nazwiska posiadacza
	 * rachunku
	 */
	public boolean setPosiadacz(String nazwaSkrocona,
			String imie,
			String nazwisko)
	{
		//w ten sposób można tworzyć tzw. domyślne wartości parametrów. Sama Java nie posiada możliwości
		//przypisania parametrom funkcji określonej wartości; można jednak, dzięki przeciążaniu, tego typu parametry
		//wstawić np. poprzez podanie literalnej wartości brakującego parametru
		return setPosiadacz(nazwaSkrocona,imie,nazwisko,false);
	}
	
	/*
	 * Funkcja działa tak, jak miała działać poprzednia; przyjmuje dodatkowy parametr, który jest
	 * bądz to podawany przez użytkownika programu, bądz domyślnie przypisywany na false
	 */
	public boolean setPosiadacz(String nazwaSkrocona,
			String imie,
			String nazwisko, 
			boolean nadpisz) {
		//jeżeli którykolwiek parametr będzie pusty - funkcja ma zwrócić false (nie zadziałała
		//I MA NIC NIE DODAWAC/ZMIENIAĆ)
		if (nazwaSkrocona.isEmpty() ||
				imie.isEmpty() ||
				nazwisko.isEmpty())
			return false;
		//jeżeli chcemy tylko nadpisać wartości, nie zmieniając pozostałych na wartość null
		if (nadpisz) {
			this.nazwaSkrocona=nazwaSkrocona; 
			this.imie=imie;
			this.nazwisko=nazwisko;
			return true;
		}
		//działanie podobne do tego, w którym metoda przyjmowała jedynie nazwaSkrocona;
		//tym razem jednak wartości niepuste przyjmują także dwa nowe parametry
		return setPosiadacz(nazwaSkrocona,
				imie,
				nazwisko,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null);
		
	}
	
	/*
	 * Metoda dodana tylko do wyeksponowania pewnej cechy parametrów w języku Java
	 * Jeżeli chcemy przyjąć większą ilość parametrów w funkcji JEDNEGO TYPU, jednak nie
	 * mamy możliwości ustalenia ile tych zmiennych będzie to możemy posłużyć się 
	 * operatorem trzech kropek (...). Dzięki niemu możliwe jest przyjęcie przez funkcję/metodę
	 * niemal nieograniczonej ilości wartości w ramach jednego parametru
	 * Sam parametr przyjmuje postać tablicy, której wielkość możemy określić np. z parametru length
	 */
	public boolean setPosiadacz(String... dane) {
		if (dane.length < 12) 
			return false;
		return setPosiadacz(dane[0], 
				dane[1], 
				dane[2],
				dane[3],
				dane[4],
				dane[5],
				dane[6],
				dane[7],
				dane[8],
				dane[9],
				dane[10],
				dane[11]
				);
	}
	
	/*
	 * Główna (podstawowa) metoda przypisująca naszym polom odpowiednie wartości; 
	 */
	public boolean setPosiadacz(String nazwaSkrocona, 
			String imie, 
			String nazwisko,
			String nazwa,
			String miejscowosc,
			String ulica,
			String kodPocztowy,
			String numerPosesja,
			String numerLokal,
			String dowodTozsamosci,
			String telefon,
			String nrKonta
			) {
		this.nazwaSkrocona=nazwaSkrocona; 
		this.imie=imie;
		this.nazwisko=nazwisko;
		this.nazwa=nazwa;
		this.miejscowosc=miejscowosc;
		this.ulica=ulica;
		this.kodPocztowy=kodPocztowy;
		this.numerPosesja=numerPosesja;
		this.numerLokal=numerLokal;
		this.dowodTozsamosci=dowodTozsamosci;
		this.telefon=telefon;
		this.nrKonta=nrKonta;
		this.saldo=.0;
		this.maksDebet=-2000.;
		return true;
	}
	
	/*
	 * Poniższe komentarze będą widoczne w podpowiedziach wyświetlanych przez środowisko Eclipse/NetBeans
	 * po naciśnięciu [CTRL+SPACE] lub podczas podpowiedzi podczas pisania fragmentu kodu
	 */
	
	/**
	 * Metoda pozwalająca na dodanie do salda dodatkowych środków
	 * @param saldo - wartość kwoty, którą doładowujemy konto
	 */
	public void dodajSaldo(double saldo) {
		//this.saldo=this.saldo+saldo;
		this.saldo+=saldo;
	}
	
	/**
	 * Metoda wywoływana np. przy wyciąganiu pieniędzy w bankomacie lub podczas płacenia kartą
	 * Jeżeli kwota do zapłąty będzie większa niż ilość kwoty na koncie otrzymamy odmowę płatności
	 * (wartość false)
	 * @param saldo - kwota, która ma zaostać wypłacona z konta
	 * @return True w przypadku wykonania transakcji (saldo pomniejszane o kwotę) lub False w przypadku odmowy
	 */
	public boolean odejmijSaldo(double saldo) {
		if (this.saldo-saldo<0)
			return false;
		this.saldo-=saldo;
		return true;
	}
	
	/**
	 * Konta bankowe mogą mieć tzw. debet. Dzięki temu możemy zadłużać nasze konto do pewnej, określonej przez bank,
	 * kwoty. Dodatkowo zakładamy, że obciążenie jednorazowe nie powinno przekraczać 50% kwoty debetu.
	 * @param saldo - kwota, która ma zaostać wypłacona z konta
	 * @return True w przypadku gdy zadłużenie konta mieści się w maksymalnym debecie, False -jeżeli przekroczymy
	 * (przekroczyliśmy) tę kwotę.
	 */
	public boolean odejmijSaldoDebet(double saldo) {
		double tmp = this.saldo-saldo;
		if (tmp>this.maksDebet*.5) {
			this.saldo-=saldo;
			return true;
		}
		return false;
	}
	
	public String pokazSaldo() {
		return Double.toString(this.saldo);
	}
}
